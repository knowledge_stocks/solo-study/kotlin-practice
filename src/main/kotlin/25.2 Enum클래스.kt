package ex25;

// Enum 클래스 내부의 값은 관용적으로 대문자를 사용한다.
// 다른 클래스들 처럼 함수도 포함할 수 있다.

fun main() {
    var state = State.SING;
    println(state);

    state = State.SLEEP;
    println(state.isSleeping());

    state = State.EAT;
    println(state.message);
}

enum class State(val message: String) {
    SING("노래하다"),
    EAT("먹다"),
    SLEEP("잔다");

    fun isSleeping(): Boolean {
        return this == SLEEP;
    }
}