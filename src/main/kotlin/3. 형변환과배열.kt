class 형변환과배열 {
}

fun main(args:Array<String>) {
    // 코틀린은 암시적 형변환을 지원하지 않는다.
    // 반드시 명시적 형변환을 해주어야 함
    var 정수32비트:Int = 3121234;
    // 형변환 함수
    var 정수64비트:Long = 정수32비트.toLong();
    var 정수16비트:Short = 정수32비트.toShort();
    var 정수8비트:Byte = 정수32비트.toByte();
    println("정수32비트: $정수32비트, 정수64비트: $정수64비트, 정수16비트, $정수16비트, 정수8비트: $정수8비트");
    // 결과: 정수32비트: 3121234, 정수64비트: 3121234, 정수16비트, -24494, 정수8비트: 82
    println("toFloat: ${정수32비트.toFloat()}, toDouble: ${정수32비트.toDouble()}, toChar: ${정수32비트.toChar()}, toChar2: ${정수32비트.toShort().toInt().toChar()}, toChar3: ${정수32비트.toByte().toInt().toChar()}");
    // Byte에서 Char로 변환하는 함수는 Deplecate 되었다는데 왜지?
    // 자바의 Char는 16비트형이기 때문에 toByte 후에 toChar는 결과가 다르게 나온다.
    // 결과: toFloat: 3121234.0, toDouble: 3121234.0, toChar: ꁒ, toChar2: ꁒ, toChar3: R


    // 배열, 한 번 설정한 배열 크기는 변경할 수 없음
    var intArr = arrayOf(1, 2, 3, 4, 5);
    var nullArr = arrayOfNulls<Int>(5); // 빈 배열
    nullArr[2] = 1234;
    println("intArr.size: ${intArr.size}, intArr[1]: ${intArr[1]}, nullArr[1]: ${nullArr[1]}, nullArr[2]: ${nullArr[2]}");
    // 결과: intArr.size: 5 intArr[1]: 2, nullArr[1]: null, nullArr[2]: 1234
}