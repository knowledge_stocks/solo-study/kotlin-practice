package ex31;

// 코루틴은 제어범위 및 실행범위를 지정할 수 있다. 이를 코루틴의 Scope라고 한다.
// 코루틴의 스코프는 GlobalScope와 CoroutineScope로 나뉜다.
// GlobalScope는 프로그램 어디서나 제어, 동작이 가능한 기본 범위이며
// CoroutineScope는 특정한 목적의 Dispatcher를 지정해서 제어 및 동작이 가능한 범위를 의미한다.
//
// Dispatcher는 3가지가 있다.
// Dispatchers.Default 기본적인 백그라운드 동작을 한다.
// Dispatchers.IO 네트워크나 디스크등 I/O에 사용한다.
// Dispatchers.Main 메인(UI) 쓰레드에서 동작한다.
//
// 코루틴은 launch 혹은 async 함수로 실행하는데
// launch는 반환값이 없는 Job 객체가 반환되고 async는 반환값을 가지는 Deffered 객체를 반환한다.

import kotlinx.coroutines.*;

fun main() {
    val scope = GlobalScope;

    val job = scope.launch {
        for(i in 1..100) {
            println(i);
        }
    }
    // 코루틴은 제어되는 스코프 또는 프로그램 전체가 종료되면 함께 종료된다.
    // 따라서 코루틴이 끝까지 실행되게 하려면 일정한 시간동안 기다려주어야 한다.
    // runBlocking을 걸어주면 앞의 launch로 실행된 코루틴이 끝날때까지 대기하는데
    // runBlocking 내부에 launch를 실행해도 비슷하게 동작한다.
    runBlocking {
        // 앞에서 코루틴이 완료되면 이어서 실행되는데 이유를 모르겠네
        val a = scope.launch {
            for(i in 101..105) {
                println(i);
                delay(500); // 루틴을 일시 대기 시킨다.
            }
        }

        val b = async {
            for(i in 106..110) {
                println(i);
                delay(5); // 루틴을 일시 대기 시킨다.
            }
            "async 종료"
        }

        println("async 시작");
        println(b);
        println("async 대기");
        println(b.await());

        println("launch 대기");
        a.join();
        println("launch 종료");

        val c = scope.launch {
            for(i in 150..155) {
                println(i);
                delay(500); // 루틴을 일시 대기 시킨다.
            }
        }

        // cancle을 통해 루틴을 취소할 수도 있다.
        // cancle을 호출하면 루틴 내부에서 delay나 yield 함수가 호출될 때 종료되거나
        // 루틴 내부 속성인 isActive를 체크해서 수동으로 종료하는 방법이 있다.
        println("launch 취소")
        c.cancel(); // delay 되는 시점에 종료된다.
        println("launch 종료");

        println("withTimeoutOrNull 시작");
        // withTimeoutOrNull로 타임아웃을 가지는 루틴을 실행할수도 있다.
        var result = withTimeoutOrNull(50) {
            for(i in 1..10) {
                println(i);
                delay(10);
            }
            "Finish";
        }

        println(result); // 타임아웃에 걸려 null이 출력된다.
    }

    // 혹은 Job.join()이나 Deferred.await() 함수로 루틴이 끝날때까지 대기하는 방법도 있다.
//    job.join(); // suspend 함수 내부에서만 호출할 수 있다고 한다.

    println("끝");
}