class 조건문과비교연산자 {
}

fun main(args:Array<String>) {
    var a = 7;

    // if문
    if(a > 10) {
        println("a is bigger than 10");
    } else if(a == 10) {
        println("a is 10");
    } else {
        println("a is smaller than 10");
    }

    // is 연산자
    if(a !is Int) {
        println("a is not integer");
    } else if (a is Int){
        println("a is integer");
    }

    // when문 실행
    doWhen(a);
    // 결과: a is 2 or 7
}

// 어떤 자료형이건 상관이 없는 경우 Any라고 표현할 수 있음
fun doWhen(a: Any) {
    // when문. 코틀린은 switch문 대신 when문을 사용한다.
    // when문은 가장 먼저 조건이 부합되는 부분만 실행한다.
    when(a) {
        1 -> println("a is 1");
        2, 7 -> {
            println("a is 2 or 7");
            println("a는 2 혹은 7")
        }
        "문자열" -> println("a is 문자열");
        !is Long -> println("a is not Long");
        is Int -> println("a is integer");
        else -> println("해당되는 조건이 없습니다."); // else 문은 항상 마지막에
    }

    // when문의 결과를 변수에 할당할 수도 있다.
    // 블록으로 감싸서 여러줄의 코드가 실행되는 경우에는 마지막 결과만 반환된다.
    var result = when(a) {
        1 -> "a is 1";
        2, 7 -> {
            "1111"
            "2222"
        }
        "문자열" -> "a is 문자열";
        !is Long -> "a is not Long";
        is Int -> "a is integer";
        else -> "해당되는 조건이 없습니다."; // else 문은 항상 마지막에
    }
    println(result);
}