class 타입추론과함수 {
}

fun main(args:Array<String>) {
    // 타입 추론이란?
    // 변수나 함수들을 선언할 때나 연산할 때 자료형을 명시하지 않아도 자동으로 추론하는 기능
    var 변수형명시:String = "문자열";
    var 문자열타입추론 = "문자열";

    var Int형타입추론 = 1234;
    var Int형타입추론2 = 0xffff;
    var Int형타입추론3 = 0b1111;
    var Long형타입추론 = 1234L;

    var Double형타입추론 = 123.4;
    var Float형타입추론 = 123.4f;

    var Boolean형타입추론 = true;

    var Char형타입추론 = 'c';

    // 함수 호출부
    println(블록형함수(2, 3, 4));
    println(단일표현식함수(2, 3, 4));
}

// 일반적인 함수 선언(블록형 함수). 반환형 생략 못 함
fun 블록형함수(a: Int, b: Int, c: Int): Int {
    return a + b + c;
}

// 단일 표현식 함수 선언. 반환형 생략 가능
fun 단일표현식함수(a: Int, b: Int, c: Int) = a + b + c;