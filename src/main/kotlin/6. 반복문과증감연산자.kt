class 반복문과증감연산자 {
}

fun main(args:Array<String>) {
    // while 문
    var a = 0;
    while(a < 5) {
        println(a);
        a++;
    }

    // do while 문
    a = 0;
    do {
        println(a++);
    } while(a < 0);

    // for 문
    println("0부터 9까지 출력됨(9포함)");
    for(i in 0..9) {
        println(i);
    }

    println("0부터 9까지 3씩 늘어가며 출력됨");
    for(i in 0..9 step 3) {
        println(i);
    }

    println("9부터 0까지 3씩 줄어가며 출력됨");
    for(i in 9 downTo 0 step 3) {
        println(i);
    }

    println("a부터 z까지 3단계씩 출력됨");
    for(i in 'a'..'z' step 3) {
        println(i);
    }
}