// val은 참조값은 바꿀 수 없지만 내부 속성값은 바꿀 수 있다.
// constants(상수)는 컴파일 시점에 결정되어 절대 바꿀 수 없는 값으로 val 앞에 const 키워드를 붙여서 선언한다.
// 상수로 선언할 수 있는 값은 기본 자료형만 해당된다.
// 상수는 클래스의 속성(멤버)이나, 지역 변수로는 선언할 수 없다.
// 반드시 companion object 안에만 선언할 수 있다.
// 상수의 이름은 모두 대문자로 표기하는 것이 관례
//
// 생각해보면 굳이 val 놔두고 const를 사용할 필요가 있는지 의문이 생기는데
// val은 런타임시 생성되고 const의 경우 컴파일 시기에 값이 결정되기 때문에 성능적으로 이득이 있다.

fun main() {
//    const val DDD = ""; // 이렇게 지역변수로는 선언할 수 없다.

    val foodCourt = FoodCourt();

    foodCourt.searchPrice(FoodCourt.FOOD_CREAM_PASTA);
    foodCourt.searchPrice(FoodCourt.FOOD_STEAK);
    foodCourt.searchPrice(FoodCourt.FOOD_PIZZA);
}

class FoodCourt {
    companion object {
        const val FOOD_CREAM_PASTA = "크림파스타";
        const val FOOD_STEAK = "스테이크";
        const val FOOD_PIZZA = "피자";
    }

    fun searchPrice(foodName: String) {
        val price = when(foodName) {
            FOOD_CREAM_PASTA -> 13000;
            FOOD_STEAK -> 25000;
            FOOD_PIZZA -> 15000;
            else -> 0
        }

        println("${foodName}의 가격은 ${price}원 입니다.");
    }
}