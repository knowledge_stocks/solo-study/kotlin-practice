package ex22;

// ==와 ===의 차이
// ==는 내용만 같다면 true ===는 객체 자체가 동일해야 true
// ==는 Any라는 최상위 클래스의 equals 함수의 값을 결정된다.
// 우리가 커스텀 클래스를 만들고 equals 함수를 정의해주면 == 연산자를 사용할 수 있나보다.

class Product(val name: String, val price: Int) {
    override fun equals(other: Any?): Boolean {
        if(other is Product) {
            return other.name == name && other.price == price;
        } else {
            return false;
        }
    }
}

fun main() {
    var a = Product("콜라", 1000);
    var b = Product("콜라", 1000);
    var c = a;
    var d = Product("사이다", 1000);

    // == 내용의 동일성 확인
    // === 객체의 동일성 확인
    println(a == b);
    println(a === b);

    println(a == c);
    println(a === c);

    println(a == d);
    println(a === d);
}