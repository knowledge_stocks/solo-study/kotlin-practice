// Set과 Map도 List와 마찬가지로 그냥 Set,Map과 Mutable이 따로 있다.

fun main() {
    val a = mutableSetOf("귤", "바나나", "키위");

    for(item in a) {
        println(item);
    }

    a.add("자몽");
    println(a);

    // set 사용법은 별다를게 없으므로 패스

    val m = mutableMapOf("레드벨벳" to "음파음파",
                         "트와이스" to "FANCY",
                         "ITZY" to "ICY");

    val (k,v) = m.toList()[0];
    println("${k},${v}");
    // map 사용법도 별다를게 없으므로 패스
}