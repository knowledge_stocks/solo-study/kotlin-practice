package ex28;

fun main() {
    val personList = listOf(Person("유나", 1992),
                            Person("조이", 1996),
                            Person("츄", 1999),
                            Person("유나", 2003));

    // name을 Key로 하는 Map으로 변환
    // 유나가 겹치기 때문에 앞에 나오는 유나가 덮어 씌워짐
    val personMap1 = personList.associateBy { it.name };
    println(personMap1);

    // birthYear Key로 하는 Map으로 변환
    val personMap2 = personList.associateBy { it.birthYear };
    println(personMap2);

    // name을 기준으로 그룹을 나누고 name을 Key로하고 해당하는 리스트를 Value로하는 Map
    val personGroupMap = personList.groupBy { it.name };
    println(personGroupMap);

    // 조건을 기준으로 두 리스트로 분리
    val (over98, under98) = personList.partition { it.birthYear > 1998 };
    println(over98);
    println(under98);
}

data class Person(val name: String, val birthYear: Int) {

}