// 제너릭은 함수와 클래스에 사용할 수 있다.
// 제너릭 선언 문법
// fun <T> genericFunc(param: T): T
// class GenericClass<T>(var pref: T)
//
// 제네릭 타입을 특정 부모의 자식으로 한정하고 싶다면
// <T: 부모클래스> 로 설정하면 된다.