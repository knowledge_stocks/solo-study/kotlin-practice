class 흐름제어와논리연산자 {
}

fun main(args:Array<String>) {
    // break와 continue
    // 사용법 다른거랑 똑같음 패스

    // 다중 반복문에서 사용이 좀 더 편리함
    // 1, 2만 출력되고 종료됨
    loop1@for (i in 1..10) {
        loop2@for (j in 1..10) {
            if(i == 1 && j == 3) {
                break@loop1;
            }
            println(i*j);
        }
    }
    
    // 논리연산자 && || ! 다 똑같음
}