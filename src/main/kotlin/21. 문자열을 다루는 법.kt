fun main() {
    val test1 = "Test.Kotlin.String";

    // 기본적인 문자 제어 함수들
    println(test1.length);
    println(test1.lowercase());
    println(test1.uppercase());

    val test2 = test1.split('.'); // 정규식이 들어가도 됨
    println(test2);

    println(test2.joinToString()); // ', '가 사이에 기본적으로 들어감
    println(test2.joinToString("-"));

    println(test1.substring(5..10)); // 10 이하
    println(test1.substring(5, 10)); // 10 미만

    println();

    // 빈 문자 체크
    val nullString: String? = null;
    val emptyString = "";
    val blankString = " \n\t\r";
    val normalString = "A";

    println(nullString.isNullOrEmpty());
    println(emptyString.isNullOrEmpty());
    println(blankString.isNullOrEmpty());
    println(normalString.isNullOrEmpty());

    println();

    // Blank에는 \t \n \r 등이 포함된다.
    println(nullString.isNullOrBlank());
    println(emptyString.isNullOrBlank());
    println(blankString.isNullOrBlank());
    println(normalString.isNullOrBlank());

    println();

    // 특저 문자 포함 여부
    var test3 = "kotlin.kt";
    var test4 = "java.java";

    println(test3.startsWith("java"));
    println(test4.startsWith("java"));

    println(test3.endsWith(".kt"));
    println(test4.endsWith(".kt"));

    println(test3.contains("lin"));
    println(test4.contains("lin"));
}