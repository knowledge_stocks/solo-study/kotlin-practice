// 코틀린에서는 변수를 선언할 때 값을 할당하지 않으면 컴파일이 되지 않는다.
// 이럴때는 lateinit 키워드를 붙여 나중에 값을 초기화할 수 있다.
// lateinit은 기본 자료형에는 사용할 수 없다. 다만 String은 사용할 수 있다.
// ::변수.isInitialized를 통해 lateinit 변수를 초기화 했는지를 확인할 수 있다.

fun main() {
    val a = LateInitSample();

    println(a.getLateInitText());

    a.text = "ㄹㄹㄹㄹ";

    println(a.getLateInitText());
}

class LateInitSample {
    lateinit var text: String;
//    var text:String; // 이런식으로 값을 할당하지 않으면 빌드가 되지 않는다.

    fun getLateInitText(): String {
        if(::text.isInitialized) {
            return text;
        } else {
            return "기본값";
        }
    }
}