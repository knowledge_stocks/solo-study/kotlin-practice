package ex25;

// Data 클래스
// 데이터를 다루는 데에 최적화 된 클래스로
// equals, hashcode, toString, copy, componentN(N번째 속성값을 반환) 함수가 자동으로 구현된다.

fun main() {
    val a = General("보영", 212);
    val b = General("보영", 212);
    println(a == b);
    println(a.hashCode());// 값이 나오긴 하지만 toString의 결과로 해싱이 이루어지기 때문에
    println(b.hashCode());// a와 b의 해시값이 다르게 나온다.
    println(a);
    println(b);

    println();

    val c = Data("루다", 306);
    val d = Data("루다", 306);
    println(c == d);
    println(c.hashCode()); // c와 d의 toString 결과가 같기 때문에
    println(d.hashCode()); // c와 d의 해시값이 동일하게 나온다.
    println(c);
    println(c.copy());
    println(c.copy("아린"));
    println(c.copy(id = 618));
    println(c.component1());
    println(c.component2());

    println();

    // 리스트에 넣어서 활용하는 방법
    val list = listOf(Data("보영", 212),
        Data("루다", 306),
        Data("아린", 618));

    for((a, b) in list) { //component1과 component2 함수를 통해 a와 b의 값이 들어오게 된다
        println("${a},${b}");
    }

    val (x,z) = c; // 요런식으로도 사용 가능하다.
}

class General(val name: String, val id: Int) {

}

data class Data(val name: String, val id: Int) {

}