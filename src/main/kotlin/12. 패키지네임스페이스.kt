package com.example.main2;

// import 할 때 as로 겹치는 이름을 다른 이름으로 치환할 수 있다.
// import asdfasfd.Animal as Animal2

// 다른 곳에 Animal이 선언되어 있지만 package를 지정해두었기 때문에 따로 선언할 수 있다.
class Animal {

}

fun main() {
    var a = Animal();
    println("asfsadf");
}