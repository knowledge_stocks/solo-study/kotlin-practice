fun main() {
    var a: String? = null;

    // ?. null safe 연산자라고 하며, 변수가 null일 경우 이어지는 명령을 실행하지 않는다.
    println(a?.uppercase());

    // ?: elvis(엘비스) 연산자라고 하며, 변수가 null일 경우 :으로 지정한 값을 대신 사용한다.
    println(a?:"default".uppercase());

    // !!. non-null assertion 연산자라고 하며, 변수가 null일 경우, null point exception을 발생시킨다.
    // 왜 굳이 !!.로 exception을 발생시키는지 이상했는데 변수의 값이 null이 분명할 경우 빌드조차 되지 않는다.
    // 사용처가 의문이긴하지만 일단 알아두자..
//    println(a.uppercase()); // 빌드조차 안 됨
    try {
        println(a!!.uppercase());
    } catch (e: NullPointerException) {
        println(e.message);
    }

    println();

    // ?.는 스코프 함수와 사용하면 좋다고 한다.
    // 스코프를 안쓰고 if문을 사용했다면 지저분해졌을 코드를 상상해보자.
    a?.run {
        // a가 null이기 때문에 스코프 전체가 실행되지 않는다.
        println(uppercase());
        println(lowercase());
    }

    a = "Kotlin Exam";
    a?.run {
        println(uppercase());
        println(lowercase());
    }

    println();
}