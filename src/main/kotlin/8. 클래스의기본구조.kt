class 클래스의기본구조 {
}

// 클래스 선언 방법 1. 생성자 한개만 가능
class Person (var name:String, val birthYear:Int) {
    fun 자기소개() {
        println("안녕하세요 ${birthYear}년생 ${name}입니다.")
    }
}

// 클래스 선언 방법 2. 생성자 여러개 가능
class Person2 {
    var name:String;
    val birthYear:Int;
    var desc:String = "없음";

    constructor(name:String, birthYear:Int) {
        this.name = name;
        this.birthYear = birthYear;
    }

    constructor(name:String, birthYear:Int, desc:String) {
        this.name = name;
        this.birthYear = birthYear;
        this.desc = desc;
    }

    fun 자기소개() {
        println("안녕하세요 ${birthYear}년생 ${name}입니다. ${desc}")
    }
}

// 1과 2 종합. 1에서 생성된 생성자는 기본생성자이고 2에서 생성된 생성자는 보조 생성자다.
class Person3 (var name:String, val birthYear:Int, var desc:String) {
    constructor(name:String, birthYear:Int) : this(name, birthYear, "없음") {
        println("보조 생성자가 사용되었습니다.")
    }

    init {
        println("${birthYear}년생 ${name}님이 생성되었습니다. 추가 설명은 ${desc}입니다.")
    }
    fun 자기소개() {
        println("안녕하세요 ${birthYear}년생 ${name}입니다.")
    }
}

fun main(args:Array<String>) {
    var 박보영 = Person("박보영", 1990);
    var 전정국 = Person("전정국", 1997);
    var 장원영 = Person("장원영", 2004);

    박보영.자기소개();
    전정국.자기소개();
    장원영.자기소개();

    println();

    var 박보영2 = Person2("박보영", 1990);
    var 전정국2 = Person2("전정국", 1997, "추가 설명");
    박보영2.자기소개();
    전정국2.자기소개();

    println();

    var 박보영3 = Person3("박보영", 1990);
    var 전정국3 = Person3("전정국", 1997, "추가 설명");
}