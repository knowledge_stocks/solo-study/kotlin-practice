// 코틀린 클래스의 기본 속성은 상속 금지이므로
// 클래스를 상속받으려면 open을 앞에 붙여주어야 한다.
open class Animal (var name:String, var age:Int, var type:String) {
    // 함수도 open으로 설정해야만 오버라이딩이 가능하다.
    open fun introduce() {
        println("저는 ${type} ${name}이고, ${age}살 입니다.")
    }
}

// Animal을 상속받는 Dog
// name과 age, type은 부모 클래스에 정의되어 있기 때문에 var이나 val 같은 변수 생성 문구를 넣지 않는다.
class Dog (name:String, age:Int) : Animal (name, age, "Dog") {
    fun bark() {
        println("멍멍");
    }
}

class Cat (name:String, age:Int) : Animal (name, age, "고양이") {
    // 함수 오버라이딩
    override fun introduce() {
        println("나는 ${type} ${name}이고, ${age}살이다냥")
    }

    fun meow() {
        println("야옹");
    }
}

fun main(args:Array<String>) {
    var dog = Dog("진돌이", 1);
    var cat = Cat("양순이", 3);

    dog.introduce();
    dog.bark();

    cat.introduce();
    cat.meow();
}