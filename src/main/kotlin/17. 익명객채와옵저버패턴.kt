package ex17;

// 옵저버. 이벤트가 일어나는 것을 감시하는 감시자
// 옵저버 패턴을 위해서는 이벤트를 수신하는 클래스와 이벤트를 발생시키고 전달하는 클래스 최소 두개의 클래스가 필요하며
// 이벤트를 받는 클래스는 그 사이를 이어줄 인터페이스를 상속해야 한다.
// 이 인터페이스를 옵저버 혹은 리스너라고 부르며, 이러한 행위를 콜백이라고 한다.

interface EventListener {
    fun onEvent(count: Int);
}

class Counter(var listener: EventListener) {
    fun count() {
        for(i in 1..100) {
            if(i % 5 == 0) {
                listener.onEvent(i);
            }
        }
    }
}

class EventPrinter: EventListener {
    override fun onEvent(count: Int) {
        print("${count}-");
    }

    fun start() {
        val counter = Counter(this);
        counter.count();
    }
}

fun main() {
    var eventPrinter = EventPrinter();

    eventPrinter.start();

    println();

    // EventPrinter 처럼 클래스를 만들지 않고 익명 클래스로 사용해도 된다.
    Counter(object: EventListener {
        override fun onEvent(count: Int) {
            print("${count}_");
        }
    }).count();
}