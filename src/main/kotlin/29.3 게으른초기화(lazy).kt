// lazy는 변수를 바로 초기화하지 않고 사용할 때까지 기다렸다가 초기화하는 속성

fun main() {
    val number: Int by lazy {
        println("초기화를 합니다.");
        7;
    }

    println("코드를 시작합니다.");

    println(number);
    println(number);
}