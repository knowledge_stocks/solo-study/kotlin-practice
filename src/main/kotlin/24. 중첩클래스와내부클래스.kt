// 중첩클래스는 단순히 클래스 안에 클래스가 있는 형태
// 중첩클래스에 접근하려면 '바깥클래스.내부클래스'와 같은 방식으로 접근하면 된다.
// 중첩클래스는 위치만 다른 클래스의 안에 있을 뿐, 외부 클래스의 내용을 공유할 수 없는 서로 별개의 클래스라고 볼 수 있다.
//
// 반면 내부(inner) 클래스는 다른 클래스 안에 'inner'라는 키워드로 선언된 클래스인데
// 이너클래스는 외부 클래스의 내용에 접근할 수 있다.
// 또한, inner 클래스를 생성하려면 바깥클래스의 객체가 필요하다.

fun main () {
    val nested = Outer.Nested();
    nested.introduce();

    println();
//    val inner = Outer.Inner(); // 이렇게는 생성할 수 없다.
    val outer = Outer();
    val inner = outer.Inner();
    val inner2 = outer.Inner();
    inner.text += 1;
    inner2.text += 2;
    inner.introduce();
    inner.introduceOuter();

    println();
    inner2.introduce();
    inner2.introduceOuter();

    outer.text += "1000"
    println();
    inner.introduceOuter();
    inner2.introduceOuter();
}

class Outer {
    var text = "Outer Class";

    // 중첩 클래스
    class Nested {
        var text = "Nested Class";

        fun introduce() {
            println(text);
        }
    }

    // 이너 클래스
    inner class Inner {
        var text = "Inner Class";

        fun introduce() {
            println(text);
        }

        fun introduceOuter() {
            println("${text}.introduceOuter: ${this@Outer.text}");
        }
    }

    fun introduce() {
        println("Outer.introduce: ${text}");
    }
}