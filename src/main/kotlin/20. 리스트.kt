// List, MutableList 두가지가 있다.
// 그냥 List는 한 번 넣은 값들은 변경이 불가능한 듯 하다.

fun main() {
    var a = listOf("사과", "딸기", "배");
    println(a[1]);

    // 리스트에서 쓰는 함수는 자스랑 비슷한 이름들로 되어 있다.
    for(fruit in a) {
        print("${fruit}:")
    }
    println();
    a.forEach({ fruit ->
        print("${fruit}:")
    });
    println();
    a.forEachIndexed({ idx, fruit ->
        print("${idx}.${fruit}:")
    })

    println();
    println();

    var b = mutableListOf(6, 3, 1);
    println(b);

    b.add(4);
    println(b);

    b.add(2,8);
    println(b);

    b.removeAt(1);
    println(b);

    b.shuffle();
    println(b);

    b.sort();
    println(b);
}