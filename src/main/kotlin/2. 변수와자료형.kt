class 변수와자료형 {
}

fun main(args:Array<String>) {
//     변수 기본 선언
    var 변수:Int = 123;
    val 상수:Int = 321;
    println("변수: $변수, 상수: $상수");

//    아래처럼 일반 변수에는 null 값을 넣을 수 없나보다. 클래스 인스턴스도 마찬가지.
//    변수 = null;
//    var 객체:HelloKotlin = null;

//    Nullable 변수
    var nullable변수:Int? = null;
    val nullable상수:Int? = null; // 상수를 null로 하는 건 의미 없는 짓이지만 어쨋든 됨
    var nullable객체:HelloKotlin? = null;

//    숫자형 자바랑 똑같음
    var 정수8비트:Byte;
    var 정수16비트:Short;
    var 정수32비트:Int;
    var 정수64비트:Long;
    var 실수32비트:Float;
    var 실수64비트:Double;

//    숫자형 표현
    정수32비트 = 1234;
    정수64비트 = 1234L; // 64바이트 크기의 정수일 경우 뒤에 L을 붙임(Long 타입 변수에 값을 넣을 때는 생략 가능하지만 웬만하면 표시하자)
    println("1234: ${1234}, 1234L: ${1234L}");

    실수32비트 = 123.5f; // 32바이트 크기의 실수일 경우 뒤에 F를 붙임
    실수64비트 = 123.5;
    실수64비트 = 1235e-1; // 필요시 지수표기법도 사용 가능
    println("123.5f: ${123.5f}, 123.5: ${123.5}, 1235e-1: ${1235e-1}");

    정수32비트 = 0xffff; // 16진수를 표현할 땐 0x
    정수32비트 = 0b1111; // 2진수를 표현할 땐 0b
    // 8진수 표현은 지원하지 않음. 안 쓰니까..
    println("0xffff: ${0xffff}, 0b1111: ${0b1111}");

    // 문자형. 코틀린은 내부적으로 문자열을 UTF-16 BE로 관리함. 따라서 1글자당 2바이트
    var 영어문자:Char = 'a';
    var 한글문자:Char = '가';
    // 이스케이프 문자들
    var 탭:Char = '\t';
    var 백스페이스:Char = '\b';
    var 첫열로이동:Char = '\r';
    var 개행:Char = '\n';
    var 작은따옴포:Char = '\'';
    var 큰따옴표:Char = '\"'; // "는 문자열의 시작과 끝으 의미하기 때문
    var 역슬래시:Char = '\\'; // \는 이스케이프 문자를 나타내기 때문
    var 달러문자:Char = '\$'; // $는 문자열 내부의 코드를 의미하기 때문
    var 유니코드문자:Char = '\uffff'; // 2바이트 = 16비트이기 때문에 16진수 4개로 표기

    // Boolean형
    var 참:Boolean = true;
    var 거짓:Boolean = false;
    println("true: ${true}, false: ${false}");

    // 문자열
    var 한줄:String = "one line string";
    var 여러줄:String = """multi
        |  line
        |string
    """
    println("한줄: $한줄");
    println("여러줄: $여러줄");
    println("여러줄.trimMargin(): ${여러줄.trimMargin()}");
}