package ex18;

// 오버라이딩 하면 부모 클래스로 업캐스팅된 상태에서도 자식의 메소드가 호출된다는 것.
// as와 is 사용법
// C#처럼 new 키워드 같은 건 없나봄

open class Drink {
    var name = "음료";

    open fun drink() {
        println("${name}를 마십니다.");
    }
}

class Cola: Drink() {
    var type = "콜라";

    override fun drink() {
        println("${name}중에 ${type}을 마십니다.");
    }

    fun washDishes() {
        println("${type}로 설거지를 합니다.");
    }
}

fun main() {
    var a = Drink();
    a.drink();

    var b:Drink = Cola();
    b.drink();
//    b.washDishes(); 이건 호출 안됨 자식으로 다운캐스트 해줘야 함

    if(b is Cola) {
        // is로 체크된 변수는 if문 안에서만 다운캐스트된 상태로 사용할 수 있다.
        b.washDishes();
    }
//    b.washDishes(); // if문 밖으로 나오면 다운캐스트가 풀림

    var c = b as Cola;
    c.washDishes();

    // as로 다운캐스트에 사용된 변수도 함께 다운캐스트 된다.
    b.washDishes();

    // as로 업캐스트 하더라도 b는 그대로 자식 타입으로 유지된다
    var d = b as Drink;
    b.washDishes();
}