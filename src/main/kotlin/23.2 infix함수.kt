package ex23;

fun main() {
    // 연산자처럼 써도 되고, 일반 함수처럼 사용해도 된다.
    println(3 multiply 2);
    println(6.multiply(2));
}

// infix함수. 마치 연산자처럼 사용할 수 있는 함수
// 함수명을 선언할 때 '자료형.함수명'으로 설정한다.
infix fun Int.multiply(x: Int): Int = this * x;