// 코틀린 클래스의 기본 속성은 상속 금지이므로
// 클래스를 상속받으려면 open을 앞에 붙여주어야 한다.
open class Animal2 (var name:String, var age:Int, open var type:String) {
    // 함수도 open으로 설정해야만 오버라이딩이 가능하다.
    open fun introduce() {
        println("저는 ${type} ${name}이고, ${age}살 입니다.")
    }
}

// Animal을 상속받는 Dog
// name과 age, type은 부모 클래스에 정의되어 있기 때문에 var이나 val 같은 변수 생성 문구를 넣지 않는다.
class Dog2 (name:String, age:Int) : Animal2 (name, age, "Dog") {
    fun bark() {
        println("멍멍");
    }
}

class Cat2 (name:String, age:Int) : Animal2 (name, age, "고양이") {
    override var type:String = "고먐미";

    // 함수 오버라이딩
    override fun introduce() {
        println("나는 ${type} ${name}이고, ${age}살이다냥")
    }

    fun meow() {
        println("야옹");
    }
}

// 추상화 클래스와 추상화 함수
abstract class Animal3 {
    abstract var name:String;
    abstract var age:Int;
    abstract var type:String;

    constructor(name:String, age:Int) {
        this.name = name;
        this.age = age;
    }

    // 함수도 open으로 설정해야만 오버라이딩이 가능하다.
    abstract fun introduce();
}

// 추상화 클래스 상속
class Cat3 (override var name:String, override var age:Int) : Animal3 (name, age) {
    override var type:String = "고양이";

    // 함수 오버라이딩
    override fun introduce() {
        println("나는 ${type} ${name}이고, ${age}살이다냥")
    }

    fun meow() {
        println("야옹");
    }
}

// 추상클래스와 인터페이스의 차이
// 코틀린에서는 인터페이스에서도 구현을 가질 수 있다.(대체 왜?)
// 다만 인터페이스는 생성자를 가질 수 없으며,
// 기본적으로 모든 멤버와 메소드가 override 가능한 상태로 설정된다.
// 그리고 인터페이스는 두개 이상 상속이 가능하다.

// 인터페이스
interface  Runner {
    fun run();
}

interface Eater {
    fun eat() {
        println("음식을 먹습니다");
    }
}

class Man : Runner, Eater {
    override fun run() {
        println("우다다다 뜁니다.");
    }
}

fun main(args:Array<String>) {
    var dog = Dog2("진돌이", 1);
    var cat = Cat2("양순이", 3);

    dog.introduce();
    dog.bark();

    cat.introduce();
    cat.meow();

    var animal2:Animal2 = cat;
    println("Animal.type: ${animal2.type}, Cat.type: ${cat.type}");
    println("Animal.type: ${(cat as Animal2).type}, Cat.type: ${cat.type}");

    println();

    var cat3 = Cat3("양순이3", 3);
    cat3.introduce();
    cat3.meow();

    println();

    // 인터페이스 사용예
    var man = Man();
    man.run();
    man.eat();
}



