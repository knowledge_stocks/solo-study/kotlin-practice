fun main() {
    val numbers = listOf(-3, 7, 2, -10, 1);

    // 각 요소를 가지고 연산한 결과를 다시 리스트로 나열한다.
    // 반환값은 Iterable로 반환해야 한다.
    println(numbers.flatMap { listOf(it * 10, it +10); });

    // 지정한 인덱스의 값을 가져오며, 만약 값이 없을 경우 람다에 지정한 값을 가져온다.
    println(numbers.getOrElse(1) { 50 });
    println(numbers.getOrElse(10) { 50 });


    val names = listOf("A", "B", "C", "D");
    // 두 배열의 동일한 인덱스를 1:1로 합치는 함수로, 결과의 길이는 작은 리스트 쪽을 따라간다.
    println(names zip numbers);
    println(names zip numbers zip numbers);
}