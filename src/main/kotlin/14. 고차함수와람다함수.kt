package ex14;

// 고차함수 = 함수를 변수(인스턴스)처럼 사용하는 것, 매개변수로 넘기거나 반환값으로 사용할 수 있다.
// 코틀린에서는 모든 함수를 변수처럼 사용할 수 있다.

fun a (str: String) {
    println("$str 함수 a");
}

// 매개변수에 함수 형식을 적을때는 람다함수 형식으로 표현한다.
// Unit = 반환값이 없다는 뜻 void랑 비슷한건가 봄
fun b (f: (String) -> Unit) {
    f("b가 호출한");
}

fun main() {
    // 함수를 고차함수로 참조하려면 ::를 앞에 붙인다.
    b(::a);

    // 람다 함수, 익명 함수 사용하기
    val c:(String) -> Unit = { str ->
        println("$str 람다함수");
        println("두번째 줄");
    }
    b(c);

    // c와 같은 람다 함수를 타입추론으로 짧게 변경한 모습
    val d = { str:String ->
        println("$str 람다함수2");
        println("두번째 줄");
    }
    b(d);

    // 이런식으로 람다함수를 바로 함수에 넘길수도 있다.
    b({str ->
        println("$str 람다함수3");
    });

    // 람다함수는 안에서 return을 사용할 수 없다.
    // return을 사용하고 싶다면 아래처럼 익명함수를 던질수도 있다.
    b(fun(str) {
        println("$str 익명함수");
        return;
    });
}