// 스코프 함수를 사용하면 인스턴스의 속성이나 메소드들을 좀 더 깔끔하게 불러 쓸 수 있다고 한다.
// 스코프 함수에는 apply, run, with, also, let 등이 있다.

class Book(var name: String, var price: Int) {
    fun discount() {
        price -= 2000;
    }
}

fun main() {
    // apply. 인스턴스를 생성한 후 변수에 담기 전에 초기화 과정을 수행할 때 많이 사용함
    // apply는 반환값으로 사용된 인스턴스를 그대로 반환한다.
    var a = Book("코틀린 기초", 10000).apply {
        name = "[초특가]" + name;
        discount();
    }

    // run. apply와 비슷하지만 반환값으로 람다 함수의 마지막 줄이 반환된다.
    println(a.run {
        "상품명: ${name}, 가격: ${price}원";
    });

    // with. run과 동일한 기능을 가지지만 인스턴스를 참조연산자(.) 대신 파라미터로 받는다는 차이점
    println(with(a) {
        "상품명: ${name}, 가격: ${price}원";
    });

    // also와 let. 각각 apply와 run과 비슷하지만 함수 내부에서 인스턴스를 참조할 때 it을 사용해서 참조해야 한다.
    // 이는 scope 바깥의 변수와 이름이 겹칠 경우를 피하기 위해 사용한다.
    var name = "가나다라";
    a = a.also {
        name = "[초초특가]" + name; // 이 name은 스코프 바깥의 "가나다라"를 가리킨다.
        it.discount();
    }

    println(a.let {
        "상품명: ${name}, 가격: ${it.price}원";
    });
}