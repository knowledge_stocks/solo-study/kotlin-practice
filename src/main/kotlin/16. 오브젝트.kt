package ex16;

// 오브젝트. 한 번 생성되면 끝이기 때문에 기본적으로 싱글턴 패턴으로 사용된다.
// 오브젝트는 최초 사용될 때 생성되고 코드 전체에서 공용으로 사용할 수 있다.

object Counter {
    var count = 0;

    fun countUp() {
        count++;
    }

    fun clear() {
        count = 0;
    }
}

// 컴패니언 오브젝트. 모든 인스턴스가 공유하는 오브젝트로. 다른 언어의 static 변수와 같은 포지션이라 생각하면 된다.
class 투표 (val name: String) {
    companion object {
        var 총투표수 = 0;
    }

    var 개별투표수 = 0;

    fun vote() {
        총투표수++;
        개별투표수++;
    }
}

fun main() {
    // 그냥 오브젝트 사용
    println(Counter.count);

    Counter.countUp();
    Counter.countUp();

    println(Counter.count);

    Counter.clear();

    println(Counter.count);

    println();

    // 컴패니언 오브젝트 사용
    var a = 투표("짜장");
    var b = 투표("짬뽕");

    a.vote();
    a.vote();

    b.vote();
    b.vote();
    b.vote();

    println("${a.name}: ${a.개별투표수}");
    println("${b.name}: ${b.개별투표수}");
    println("총투표수: ${투표.총투표수}");
}