// collection.any { it == 0 } // 하나라도 조건에 맞을경우 true
// collection.all { it == 0 } // 모두 조건에 맞을 경우 true
// collection.none { it == 0 } // 조건에 맞는게 없을 경우 true
// collection.first() // 가장 처음 요소
// collection.first { it > 0 } // 조건에 맞는 가장 처음 요소
// collection.last도 마찬가지
// first는 find, last는 findLast로 대체할 수 있으며
// first와 last는 값을 찾지 못할 경우 NoSuchElementException을 발생시킨다.
// firstOrNull과 lastOrNull은 값을 찾지 못할 경우 null을 반환한다.
// collection.count() // 그냥 모든 값의 개수
// collection.count { it > 0 } 조건에 맞는 개수