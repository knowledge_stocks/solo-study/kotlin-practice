fun main() {
    read(7);
    read("감사합니다.");

    deliveryItem("짬뽕"); // 이 경우 default 값이 적은 것부터 탐색한다.
    deliveryItem("책", 3);
    deliveryItem("노트북", 30, "학교");

    // 중간의 default 값은 그대로 사용하고 뒷 부분에만 매개변수를 넘기고 싶다면
    // argument의 이름을 지정해서 매개변수를 전달하면 된다.
    deliveryItem("선물", destination = "학교");

    // vararg 사용
    println(sum(1,2,3,4,5));
    println(sum(1,4,8));
}

// 오버로딩. 함수명은 같지만 arguments가 다른 경우
fun read(x: Int) {
    println("숫자 $x 입니다.")
}

fun read(x: String) {
    println(x)
}

// arguments에 기본값 설정
fun deliveryItem(name: String, count: Int = 1, destination: String = "집") {
    println("${name}, ${count}개를 ${destination}에 배달하였습니다.");
}

fun deliveryItem(name: String, count: Int = 100) {
    deliveryItem(name, count, "우주");
}

// vararg. 매개변수를 특정 개수가 아닌 그냥 여러개 받고 싶은 경우
// vararg는 반드시 맨 뒷부분에 위치해야 함
fun sum(vararg numbers: Int): Int {
    var sum = 0;

    //vararg로 받아온 값은 배열처럼 참조할 수 있다.
    for(n in numbers) {
        sum += n;
    }

    return sum;
}